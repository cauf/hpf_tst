from urllib.parse import (
    ParseResult,
    urlparse,
    urlunparse,
)
from flask import Response, jsonify


simbols = [chr(s) for s in range(97, 123)]


def get_url(uri:str) -> str:
    up:ParseResult = urlparse(uri)
    return f'{up.scheme}://{up.netloc}'


def get_name(uri:str) -> str:
    netloc = urlparse(uri).netloc
    name = ''.join([s if s in simbols else '_' for s in netloc])
    return name


def send_error(msg:str, status:int) -> Response:
    resp = jsonify({
        'type': 'error',
        'message': msg,
    })
    resp.status_code = status
    # resp.status = 'Not Found'
    return resp
