from yaml import load, FullLoader
from pathlib import Path


BASE_PATH:Path = Path(__file__).resolve().parent

SETTINGS_FILE:Path = BASE_PATH / 'settings.yaml'


with open(SETTINGS_FILE, mode='r', encoding='utf-8') as sfl:
    _raw_data:str = sfl.read()

_data:dict = load(_raw_data, FullLoader)


DB_PATH:Path = Path(_data['db_path'])
if not DB_PATH.is_absolute():
    DB_PATH = (BASE_PATH / DB_PATH).resolve()

DEPTH:int = int(_data['depth'])

DOWNLOAD_PATH:Path = Path(_data['download_path'])
if not DOWNLOAD_PATH.is_absolute():
    DOWNLOAD_PATH = (BASE_PATH / DOWNLOAD_PATH).resolve()
if not DOWNLOAD_PATH.exists():
    DOWNLOAD_PATH.mkdir()

CELERY_BROKER:str = _data['celery']['broker']
CELERY_BACKEND:str = _data['celery']['backend']
