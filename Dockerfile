FROM python:3.9-alpine3.12

RUN mkdir /usr/src/hpf_tst

WORKDIR /usr/src/hpf_tst

COPY . .

RUN apk update \
    && apk --no-cache add ca-certificates openssl wget \
    && update-ca-certificates 

RUN pip install wheel gunicorn

RUN pip install -r requirements.pip

EXPOSE 8000

# CMD [ "gunicorn", "-b 0.0.0.0:8000", "main:app" ]
