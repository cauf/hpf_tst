from flask import (
    Flask, 
    request,
    Response,
    jsonify,
    url_for,
    send_from_directory,
)
from tasks import download
from storage import (
    set_site, 
    get_site,
    set_celery_task,
    get_celery_task,
    verify_id,
)
from celery.result import AsyncResult
from logging import debug, basicConfig, DEBUG
from utils import send_error


app = Flask(__name__)
basicConfig(filename="app.log", level=DEBUG, format='[%(filename)s] [%(levelname)s] [%(funcName)s()] [%(lineno)d]\t[%(message)s]')


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            if 'uri' in data.keys():
                uri = data['uri']
                task_id = set_site(uri)
                celery_task = download.delay(task_id)
                set_celery_task(task_id, celery_task.task_id)
                task_status = AsyncResult(celery_task.task_id)
                resp = jsonify({
                    'task_id': task_id,
                    'uri': uri,
                    'task_status': task_status.state,
                })
                resp.status_code = 202
                return resp
    elif request.method == 'GET':
        debug('inter to GET "/"')
        if 'task_id' in request.args.keys():
            task_id = int(request.args['task_id'])
            debug(f'Getting task_id={task_id}')
            if verify_id(task_id):
                uri, folder, celery_task = get_site(task_id)
                debug(f'Getting uri="{uri}", folder="{folder}", celery_task="{celery_task}"')
                task_status = AsyncResult(celery_task)
                debug(f'Getting task_status="task_status.state"')
                resp_data = {
                    'task_id': task_id,
                    'uri': uri,
                    'task_status': task_status.state,
                }
                if task_status.state == 'SUCCESS':
                    resp_data['result'] = url_for(
                        'give_result', 
                        task_id = task_id,
                    )
                resp = jsonify(resp_data)
                debug(f'Create response={resp}')
                resp.status_code = 200 if task_status.state == 'SUCCESS' else 102
                debug(f'Set status code={resp.status_code}')
                return resp
            else:
                debug('Return error')
                return send_error(
                    f'Task with id {task_id} not detected',
                    404,
                )
    return send_error(
        'The POST method with the URL specified in the request body or the '
        'GET method with the task_id parameter is expected.',
        405,
    )


@app.route('/result/<int:task_id>', methods=['GET',])
def give_result(task_id):
    if request.method == 'GET':
        if verify_id(task_id):
            _, folder, _ = get_site(task_id)
            file_name = f'{folder.stem}.zip'
            return send_from_directory(
                str(folder),
                file_name,
            )
        else:
            return send_error(
                f'Task with id {task_id} not detected',
                404,
            )
    return send_error(
        'The GET method with the task_id parameter is expected.',
        405,
    )
    