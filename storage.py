from sqlite3 import connect
from settings import (
    DB_PATH,
    DOWNLOAD_PATH,
)
from pathlib import Path
from utils import (
    get_name,
    get_url,
)


db = connect(DB_PATH)
cur = db.cursor()

sql_create_table = """
    create table if not exists tasks(
        id integer primary key autoincrement,
        uri text not null default '',
        folder text not null default '',
        celery_task text not null default ''
    )
"""

sql_insert_uri = """
    insert into tasks(
        uri
    ) values (?)
"""

sql_update_folder = """
    update tasks
    set folder = ?
    where id = ?
"""

sql_select_task_id = "select id from tasks where uri=?"

sql_count_task_id = "select count(id) from tasks where id = ?"

sql_select_site = """
    select 
        uri,
        folder,
        celery_task
    from tasks
    where id = ?
"""

sql_update_celery_task = """
    update tasks 
    set celery_task = ? 
    where id = ?
"""


cur.execute(sql_create_table)


def set_site(uri:str) -> int:
    cur.execute(sql_insert_uri, (uri, ))
    db.commit()
    cur.execute(sql_select_task_id, (uri, ))
    task_id = int(cur.fetchall()[-1][0])
    folder = DOWNLOAD_PATH / f'{task_id}_{get_name(uri)}'
    cur.execute(sql_update_folder, (str(folder), task_id))
    db.commit()
    return task_id


def get_site(task_id:int) -> tuple:
    cur.execute(sql_select_site, (task_id, ))
    res = cur.fetchone()
    return res[0], Path(res[1]), res[2]


def set_celery_task(task_id:int, celery_task:str) -> None:
    cur.execute(sql_update_celery_task, (celery_task, task_id))


def get_celery_task(task_id:int) -> str:
    cur.execute(sql_select_site, (task_id,))
    res = cur.fetchone()
    return res[2]


def verify_id(task_id:int) -> bool:
    cur.execute(sql_count_task_id, (task_id, ))
    res = cur.fetchone()
    return res[0] > 0

