from celery import Celery
from settings import (
    CELERY_BROKER,
    CELERY_BACKEND,
)
from settings import DEPTH
from storage import get_site
from subprocess import call
from zipfile import ZipFile


app = Celery(
    'hpf_tst',
    broker=CELERY_BROKER,
    backend=CELERY_BACKEND,
)

@app.task
def download(task_id:int):
    uri, folder, _ = get_site(task_id)
    if not folder.exists():
        folder.mkdir()
    call(['wget', '-r', '-k', '-p', '-E', '-nd', '-nc', '-np', 
        '-l', str(DEPTH), '-P', str(folder), uri])
    print('WGET download done!')
    arch_path = folder / f'{folder.stem}.zip'
    arch = ZipFile(arch_path, 'w')
    for fl in folder.iterdir():
        if fl != arch_path:
            arch.write(fl)
    arch.close()

